# So here is a problem.
#
# IMPORTED_* properties of targets exist in multiple versions:
# config-less, IMPORTED_PROPERTY (e.g. IMPORTED_LOCATION)
# and config-specific, e.g. IMPORTED_PROPERTY_RELEASE.
#
# CMake looks for properties in this order by default for e.g. RelWithDebInfo build type:
# - IMPORTED_LOCATION_RELWITHDEBINFO,
# - IMPORTED_LOCATION,
# - if not found, takes hint from IMPORTED_CONFIGURATIONS, e.g. DEBUG;RELEASE
# - IMPORTED_LOCATION_DEBUG,
# - IMPORTED_LOCATION_RELEASE.
# This search order is not documented anywhere, see below.
#
# First, the base property is searched, which is
# - IMPORTED_LIBNAME for INTERFACE libraries,
# - IMPORTED_OBJECTS for OBJECT libraries,
# - any of IMPORTED_LOCATION or IMPORTED_IMPLIB where applicable (mainly for DLLs),
# - IMPORTED_LOCATION for regular libraries.
#
# A minor inconveniece is if e.g. IMPORTED_IMPLIB_RELEASE is applicable, and it isn't found,
# but IMPORTED_LOCATION_RELEASE is found, then CMake will happily use the RELEASE
# properties. It will be the active configuration, regardless that CMake needs to use
# *only* the IMPLIB at link-time (which is missing), and has nothing to do with
# the LOCATION, which is a requirement only at run-time. So it will try to link to
# a property value like foo-NOTFOUND, causing build-time error (!) without a single warning.
#
# Then, whichever version of the base property is found, the related IMPORTED_*
# properties will be apploied of the same configuration, so e.g.
# IMPORTED_LOCATION_RELEASE implies IMPORTED_SONAME_RELEASE. [1]
#
# This search order is fragile as f*ck, because IMPORTED_CONFIGURATIONS is
# treated with order as priority. [2]
#
# The problem is that the order is non-deterministic.
# Normally, fooTargets-<CONFIG>.cmake adds <CONFIG> to IMPORTED_CONFIGURATIONS,
# and fooTargets-*.cmake files are globbed from fooConfig.cmake. Starting from CMake 3.6,
# the GLOB order is lexicographical, giving you this order if all configs are present:
# DEBUG;MINSIZEREL;RELEASE;RELWITHDEBINFO.
# So in case of RelWithDebInfo build type, and DEBUG;RELEASE IMPORTED_CONFIGURATIONS (co-installed),
# the DEBUG will take precedence, which doesn't look right: Release seems more fitting.
# But this is just a side effect of the auto-generated config files and GLOB behavior,
# and does not apply if the IMPORTED_CONFIGURATIONS is filled in *any other way*.
# An other way would be Find modules, which are hand-written, mostly in CMake's
# system modules, but is routinely written and used by CMake users.
# A concrete example, showing that this is not just theoretical, is FindZLIB.cmake [3],
# as of now (2021, CMake 3.20) if DEBUG;RELEASE libraries are available,
# fills it in RELEASE;DEBUG order. So if find_package(ZLIB) happens to find the CONFIG
# module, the lexicograhpical order will give DEBUG libraries, but if MODULE is found,
# the RELEASE takes precedence. This behavior dates back to 2.6 [4] and is still present [5].
# Minor detail: prior to 3.6 the GLOB order was fully undeterministic [6], probably
# kept the order that the filesystem gave, so in case of CONFIG, a butterfly's wing flaps
# decided whether you linked DEBUG or RELEASE zlib.
#
# Luckily for us, the MAP_IMPORTED_CONFIG_<CONFIG> property overrides this search order.
# It lets you customize the search order described above, but if you specify ANYTHING in there,
# IMPORTED_CONFIGURATIONS will be fully ignored [7], so it will suddenly become deterministic.
# Note that as of now (2021, CMake 3.20) this is still undocumented, including the full search
# order and the ignoring, appears neither on MAP_IMPORTED_CONFIG_<CONFIG>'s [8] nor
# IMPORTED_CONFIGURATIONS' documentation page [9].
#
# The global variable CMAKE_MAP_IMPORTED_CONFIG_<CONFIG>'s value (at the time of add_library())
# is the default value for MAP_IMPORTED_CONFIG_<CONFIG>, and applied for imported targets only.
#
# Note that this is a LIST with order as priority.
# For Release, we'll set this to "Release;MinSizeRel;RelWithDebInfo;"
#
# The empty string item in the list stands for the configless property: IMPORTED_PROPERTY.
# This means when the CMake "generating step" is looking for e.g. the imported
# library's location, it will consider the location properties in this order:
# - IMPORTED_LOCATION_RELEASE,
# - IMPORTED_LOCATION_MINSIZEREL,
# - IMPORTED_LOCATION_RELWITHDEBINFO,
# - IMPORTED_LOCATION.
#
# Note that if you omit Release from the list, then the *_RELEASE property will be ignored,
# even if the build type is Release. Yes, f*ck logic, and the documentation for this is terrible.
# The effective default value for this property is "Release;" (note the empty string in the end).
# Of course, this isn't documented either, and the actual default is undefined.
#
# Another minor detail is: other properties relevant to IMPORTED libraries that don't
# start with IMPORTED_ don't have a variant for _<CONFIG>, so if you want e.g.
# different (imported) compile definitions per configs with INTERFACE_COMPILE_DEFINITIONS [10],
# your best try is a generator expression depending on CONFIG [11],
# but, as you probably expected by now, the CMAKE_BUILD_TYPE/CONFIG will not always
# be the one that corresponds to the actually picked binaries (LOCATION).
# The MAP_IMPORTED_CONFIG_<CONFIG> is honored, but not IMPORTED_CONFIGURATIONS.
# So you might get IMPORTED_LOCATION_RELEASE with -DFOO_DEBUG [12], possibly breaking ABI,
# causing hard-to-debug crashes.
# If you are a library consumer, you should always define MAP_IMPORTED_CONFIG_* for this reason,
# and if you are a library producer, you'd better not use generator expressions
# depending on CONFIG, because it works reliably only if the consumer defines
# MAP_IMPORTED_CONFIG_* and is unreliable by default.
#
# If you need to read this comment, please check the official documentation, probably
# they have updated it (since 3.20) to be human consumable:
# https://cmake.org/cmake/help/latest/prop_tgt/MAP_IMPORTED_CONFIG_CONFIG.html
# And please increment the following counter:
#     total_hours_wasted_here = 20
#
# This effectively tells CMake that if we're building Release, we can deterministically
# consume packages that were built as MinSizeRel or RelWithDebInfo too.
# This property is the *only reliable* way (as of 2021, CMake 3.20) that CMake lets you
# consume a package that is built with different build type than the consumer's build type,
# because the (automatically generated) exported fooTargets-relwithdebinfo.cmake
# only sets RELWITHDEBINFO properties, so by default, consuming with other build types can fail
# because e.g. the IMPORTED_LOCATION_RELEASE is empty, and the fallback mechanism is fragile.
#
# References:
#   Note that many of these are RTFS (Read The F***ing Source).
#   In those cases, I linked a pinned reference (at the time of writing),
#   and noted method name, for better future trackability if you want to check latest master.
#   [1] cmGeneratorTarget::ComputeImportInfo: https://github.com/Kitware/CMake/blob/a5678e46f8fd6a1f4081165b770a6249b3225e88/Source/cmGeneratorTarget.cxx#L6943
#   [2] cmTarget::GetMappedConfig: https://github.com/Kitware/CMake/blob/9b96fbc358aba8110ba09d3ad2262223ec0f0167/Source/cmTarget.cxx#L2135
#   [3] FindZLIB.cmake, ZLIB::ZLIB: https://github.com/Kitware/CMake/blob/a5678e46f8fd6a1f4081165b770a6249b3225e88/Modules/FindZLIB.cmake#L137
#   [4] cmTarget.cxx:2938, cmTarget::ComputeImportInfo: https://github.com/Kitware/CMake/commit/5594ad488576a77d9c6b8c3c1999a04fb4e6867d#diff-9b59ba740836c6609240b56090a61662e97cc17b82dffc9f683ec64dc5e06680R2938
#   [5] cmTarget::GetMappedConfig: https://github.com/Kitware/CMake/blob/9b96fbc358aba8110ba09d3ad2262223ec0f0167/Source/cmTarget.cxx#L2240
#   [6] file(GLOB): https://cmake.org/cmake/help/v3.20/command/file.html#filesystem
#   [7] cmTarget::GetMappedConfig: https://github.com/Kitware/CMake/blob/9b96fbc358aba8110ba09d3ad2262223ec0f0167/Source/cmTarget.cxx#L2208
#   [8] https://cmake.org/cmake/help/v3.20/prop_tgt/MAP_IMPORTED_CONFIG_CONFIG.html
#   [9] https://cmake.org/cmake/help/v3.20/prop_tgt/IMPORTED_CONFIGURATIONS.html
#   [10] https://cmake.org/cmake/help/v3.20/prop_tgt/INTERFACE_COMPILE_DEFINITIONS.html
#   [11] https://cmake.org/cmake/help/v3.20/manual/cmake-generator-expressions.7.html#variable-queries
#   [12] https://gitlab.kitware.com/cmake/cmake/-/issues/15142
set(CMAKE_MAP_IMPORTED_CONFIG_RELEASE Release MinSizeRel RelWithDebInfo "")
