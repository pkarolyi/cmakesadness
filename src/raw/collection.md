- You can't install a target in CMake from a different directory, only starting from 3.11
- The order of install commands are unspecified, except subdirectories, they are processed after the upper directory. Thus, postinstall steps have to be in a separate folder.
ExternalProject_Add won't see the environment variables set in the CMakeLists.txt. If you need that, use env executable, which works the same way from cmd (if Cygwin is in the - PATH), and bash.
- Imported libraries cannot have aliases. This is an arbitrary restriction, but there is a possible workaround: duplicate the library and copy all properties.
CMake's execute_process may fail on a cygwin symlink, ran into it when pkg-config was a symlink. In this case, the pkg-config version check fails silently but version string - remains empty.
- We use bash -c "readlink -f <file>" to resolve the symlink. github.com/DynamoRIO/dynamorio/blob/master/suite/tests/CMakeLists.txt#L265
pkg-config has no straightforward way to exclude all system packages. However, checking its source code revealed that if PKG_CONFIG_LIBDIR env var is set, system packages are - skipped, and invalid paths (e.g. "1") are tolerated.
CMake's pkg_check_modules has no way to add extra arguments to the pkg-config calls. However, adding --define-variable=prefix=<prefix> to the executable name itself works. If - you add --define-prefix instead, it won't work, because that is incompatible with --exists that CMake also calls internally.
Old versions of FindOpenSSL.cmake forget to add -ldl to the link flags for openssl static libraries. Its direct dependencies usually call find_package(OpenSSL) in a way that - there's no direct way to fix this, only workarounds.
FindOpenSSL.cmake and some other modules specify libraries by absolute path, which may get into the exported cmake files, causing trouble relocating the package. You can use - postinstall+sed to get rid of them.
- FindZLIB.cmake is broken on Windows for static zlib library called zlibstaticd, fixed only in 3.15. 
- CMake's try_run doesn't handle default values for cross-compiling (when we can't just run executables). You can set the result variables to cache, that short-circuits.
- Toolchain has to be set before project(), and CMake compiler-dependent variables are available only after project().
CMake install(EXPORT ...) may generate wrong directory structure: the export foobarConfig.cmake files must be in a folder named exactly foobar (for specific installation - conventions), but install(EXPORT ...) may not follow the file or the folder naming. Can be fixed by postinstall.
- CMake's execute_process command cannot have quote inside an argument (at least on Windows). It just can't.
- If you need a backslash in CMake's execute_process command argument, it needs to be escaped twice on Windows (i.e. \\\\) but once on Linux (i.e. \\).
- Some important variables, e.g. CMAKE_STATIC_LIBRARY_SUFFIX are not available at install-time.
The CMake property INTERFACE_LINK_LIBRARIES doesn't keep the linking order. This also messes up CMake's pkg-config integration, pkg_check_modules() with multiple libraries in - the .pc files.
Some find_package(MODULE) and pkg_check_modules() calls create UNKNOWN libraries, which is problematic when we know its exact type (can't change after creation), and we want to - set e.g. properties that are for SHARED libraries, e.g. IMPORTED_NO_SONAME. 
find_package, find_library, stb. puts results in the cache, so if you change their arguments, re-run CMake, the old values get stuck in the cache. You need to clean the cache - manually in these cases.
install(... DESTINATION "/absolute/dest") does silently not work in scenarios where the installation destination (given on the command line, to cmake --install) is not - CMAKE_INSTALL_PREFIX. Happens regularly for CPack. Do not write absolute paths there!
- If quoting is wrong for ExternalProject_Add's commands, weird things can happen: steps skipped, random error messages.
- If you call a function as foo(A "") CMake will tell you ARGC is 1, and discards the empty string for ARGV2.
- If you call set(<var> <value> PARENT_SCOPE) CMake will only set the value on the parent scope, not the one in it is called.